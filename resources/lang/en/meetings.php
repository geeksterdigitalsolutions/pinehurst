<?php

return [
    'title' => 'Meetings and Seminars',
    'content' => 'We have venues that can serve from 30 to 500 people. The meeting halls are the Governor Room, Meadow Room, Palm View Ballroom, and the Forest room which are equipped with everything you need to make your meetings or seminars that you ever wished for.',
    'price' => 'Rate',
    'wedding-service' => [
        'title' => 'Wedding Service',
        'content' => 'Looking for the perfect wedding venue in your dreams? We have professional organizers that are ready to consult, plan, and take care of the whole wedding. We make sure every step and detail will be organized with utmost care to create the perfect wedding in your dreams.',
        'promotion' => 'Promotion !!!!!! <br>
        Engagement Ceremony – Holy Water Pouring Ceremony for 50 guests from 32,000 Baht to only 26,999 Baht! <br>
        More details and info at 02-5168679-84, 088-844-4888, 088-888-8000'
    ],
    'fullday' => 'Full Day',
    'halfday' => 'Half Day',
];
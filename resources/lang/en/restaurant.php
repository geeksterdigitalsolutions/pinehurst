<?php

return [
    'content' => 'Our dining halls uses the best ingredients to make the tastiest dishes, as well as great service for our special guests, to make eating a meal in our dining hall one of life’s best experiences.',
    'tab' => [
        'holeinone' => 'Hole in One',
        '19' => '19 th HOLE',
        'china' => 'Chinese Dining Hall'
    ]
];
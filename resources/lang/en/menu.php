<?php
return [
    'home' => 'HOME',
    'golf' => 'GOLF',
    'about' => 'ABOUT US',
    'golf_course' => 'GOLF COURSE',
    'room' => 'LODGINGS',
    'meeting_room' => 'MEETING ROOM',
    'restaurant' => 'RESTAURANT',
    'facilities' => 'FACILITIES',
    'gallery' => 'GALLERY',
    'contact' => 'CONTACT'
];
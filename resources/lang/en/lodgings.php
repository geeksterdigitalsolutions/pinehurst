<?php

return [
    'title' => 'Accomodations & Lodgings',
    'content' => 'Pinehurst Golf Club rooms and houses combines a total of 278 units divided into three zones, the Palm View, the Pinehurst Lodge, and the Garden Home. Our staffs are ready to welcome you in and care for you wholeheartedly. These rooms are equipped with amenities and facilities to provide the best possible experience for you, such as wifi, jacuzzis, smoke-free environment, air conditioning, and even call alarm services. With maximum security we are sure to make your stay here remarkable and unforgettable.',
    'plamview' => [
        'title' => 'Palm View',
        'content' => 'Palm View 141, with golf course view from within the room, guests are mesmerized by the spectacle of Pinehurst Golf Club',
    ],
    'pinehurstlodge' => [
        'title' => 'Pinehurst Lodge',
        'content' => 'Pinehurst Lodge has 79 rooms with pool-view and greens-view. With every amenities you would ever need, we are sure your stay here pleasant and satisfying.',
    ],
    'gardenhome' => [
        'title' => 'Garden Home',
        'content' => 'Garden Home, with 58 rooms designed as a house in the middle of a garden. With refreshing air and cool breeze, we are sure you will feel like living in your own house',
    ]
];
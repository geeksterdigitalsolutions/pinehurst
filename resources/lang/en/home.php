<?php
return [
    'ph_article' => [
        'title' => 'Pinehurst Golf Club',
        'content' => 'Located near the Motorway, tollway, Donmueng Airport, Talad Thai, and many more iconic locations, Pinehurst Golf Club’s strategic location makes it convenient for golfers across the country. Our golf club offers a variety of services that are sure to make your experience special and unforgettable. Our golf club also offers night club especially for golfers who do not have time during the day. We use cool light bulbs that emits white light that simulates natural sun lighting, which are adjustable according to the weather. With great greens, perfect location, and wide variety of services, and many more reasons contributes to Pinehurst Golf Club famous among golfers.'
    ]
];
<?php
return [
    'amenities' => [
        'title' => 'Amenities',
        'bed' => 'Beds Twin/Double',
        'refrigerator' => 'Refridgerator',
        'tv' => 'TV',
        'air' => 'Air Conditioner',
        'bathroom' => 'Private Bathroom',
        'clothesline' => 'Clothesline',
        'wardrobe' => 'Wardrobe',
        'slipper' => 'House Slippers',
        'phone' => 'Landline Phone',
        'wifi' => 'Wifi',
        'heater' => 'Water Heater',
        'dining' => 'Dining Table',
        'furniture' => 'Furniture'
    ],
    'extra_service' => [
        'title' => 'Extra Services',
        'first' => 'We have shuttle bus service from Pinehurts Golf Club to Future Park Rangsit and Future Park Rangsit to Pinehurst Golf Club',
        'second' => 'Room Service 06.00 - 22.00'
    ],
    'securities' => [
        'title' => 'Securities',
        'first' => 'Maximum security officers in every locations',
        'second' => 'CCTV'
    ]
];
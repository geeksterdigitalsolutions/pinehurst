<?php

return [
    'content' => 'Other than our golf courses we also offer swimming pools, fitnesses, saunas, and Thai massage. After a long day of golfing we are happy to make your stay full of happiness with our full ranges of services we compiled in Pinehurst Golf Club.',
    'tab' => [
        'sport-club' => 'Sport Club',
        'massage' => 'Massage Services',
        'shop' => 'Store'
    ],
    'sport-club' => [
        'content' => 'Whole Day 250 Baht per person (Only Sport Club and Sauna) <br> Open 06.00 - 20.00'
    ],
    'massage' => [
        'body' => 'Body Massage',
        'feet' => 'Foot Massage',
        'oil' => 'Oil Massage'
    ],
    'shop' => [
        'title' => 'Convenience Store',
        'content' => 'Within Pinehurst Golf Club we have a minimart with standard prices and items <br> Monday - Friday On 13.00 - 24.00 <br> Saturday - Sunday On 10.00 - 24.00 น.'
    ]
];
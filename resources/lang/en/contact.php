<?php
return [
    'title' => 'Travel route to Pinehurst Golf Club',
    'content' => 'Drive through the Donmueng Tollway to Rangsit and after exit drive straight another ten minutes and you will see the Pinehurst Golf Course sign to the left of Bangkok University ',
    'info' => 'Locate Us : 146/4 Moo 17 Phahonyothin Road, Tambon Khlong Nung, Amphoe Khlong Luang, Pathumthani 12120 <br>
    Call Us : 02-5168679-84, 088-844-4888, 088-888-8000 <br>
    Open Hours : <span class="text-success">Always Open</span>'
];
<?php

return [
    'type' => [
        'thai' => 'Thai Citizens',
        'foreigner' => 'Foreigner'
    ],
    'week_day' => 'Weekdays',
    'holiday' => 'Holidays',
    'morning' => 'Before 11.00',
    'after_noon' => 'After 11.00',
    'after_15oclock' => 'After 15.00',
    'holes' => ':hole Holes',
    'price' => ':price Baht',
    'person_unit' => ':price Baht / Person',
    'person_pack_unit' => ':price Baht for :person person',
    'register' => 'Register',
    'guest-member' => 'GUEST MEMBER',
    'register' => [
        'tab' => 'Register',
        'content' => 'Yearly member (Thai citizen only)<br>
        First member cost 65,000 Baht/Year/Person<br>
        !!! Promotion !!! Now just 58,500 บาท
        <br><br>
        Yearly member (Foreigner only)<br>
        First member cost 99,500 Baht/Year/Person
        <br><br>
        Ask for information <br>
        Call. 02-5168679-84, 088-844-4888, 088-888-8000'
    ],
    'guest-member' => 'GUEST MEMBER',
    'golf-set' => ':price Baht / SET',
    'shoe' => 'Golf Shoe',
    'umbella' => 'Umbella',
];
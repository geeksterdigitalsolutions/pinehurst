<?php

return [
    'baht' => 'Baht',
    'person' => ':person Guest(s)',
    'hour' => 'An hour',
    'hours' => ':h Hours'
];
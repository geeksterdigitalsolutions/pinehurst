<?php

return [
    'baht' => 'บาท',
    'person' => ':person ท่าน',
    'hour' => 'ชั่วโมง',
    'hours' => ':h ชั่วโมง'
];
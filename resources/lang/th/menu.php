<?php
return [
    'home' => 'หน้าแรก',
    'golf' => 'กอล์ฟ',
    'about' => 'เกี่ยวกับเรา',
    'golf_course' => 'GOLF COURSE',
    'room' => 'ห้องพัก',
    'meeting_room' => 'ห้องประชุม',
    'restaurant' => 'ห้องอาหาร',
    'facilities' => 'FACILITIES',
    'gallery' => 'แกลลอรี่',
    'contact' => 'ติดต่อเรา'
];
<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container-fluid" style="border-bottom: 0.1mm solid rgb(199,199,199);">
        <button type="button" id="sidebarCollapse" class="btn">
            <i class="fas fa-bars text-success"></i><br>
            <span>MENU</span>
        </button>
        
        <div class="w100 text-center">
            <img src="{{ URL::asset('img/logo-cutout.png') }}" alt="" height="60" style="width: 60px;">
        </div>

        <button type="button" id="search" class="btn">
            <i class="fas fa-search fa-2x text-success"></i>
        </button>
    </div>
</nav>
<div class="row d-none d-sm-block top-menu">
    <div class="col-12" style="padding: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ URL::asset('/home') }}" class="nav-link">{{ trans('menu.home') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/golf') }}" class="nav-link">{{ trans('menu.golf') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/about') }}" class="nav-link">{{ trans('menu.about') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/golf-course') }}" class="nav-link">{{ trans('menu.golf_course') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/room') }}" class="nav-link">{{ trans('menu.room') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/meeting-room') }}" class="nav-link">{{ trans('menu.meeting_room') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/restaurant') }}" class="nav-link">{{ trans('menu.restaurant') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/facilities') }}" class="nav-link">{{ trans('menu.facilities') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/gallery') }}" class="nav-link">{{ trans('menu.gallery') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::asset('/contact') }}" class="nav-link">{{ trans('menu.contact') }}</a>
                </li>                
            </ul>
        </nav>
    </div>
</div>

@extends('layouts.master')

@section('body')
<body>
    <div class="wrapper">
        <!-- Sidebar -->
        <nav id="sidebar">
            @include('components.sidemenu')
        </nav>
        <!-- Page Content -->
        <div id="content" class="container-fluid" style="padding: 0px;">
    
            @include('layouts.header')
            
            @yield('content')

            @include('layouts.footer')
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ URL::asset('js/flexCarousel.min.js') }}"></script>
    <script src="{{ URL::asset('js/fotorama.js') }}"></script>
    <script>
        $(document).ready(function () {

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            $('.carousel').carousel()
            $('.flexCarousel').flexCarousel();
        });
    </script>
    @section('script') 
    @show
</body>
@endsection
<a href="{{ $link }}">
    <div id="event-tag">
        <div class="card">
            <div class="tag text-uppercase {{ $tag }}">{{ $tag }}</div>
            <img class="card-img-top" src="{{ $img }}">
            <div class="card-body">
                <p class="card-text indent-50">{{ $slot }}</p>
            </div>
        </div>
    </div>
</a>
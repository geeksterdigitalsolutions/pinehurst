<div class="article-box">
    <img class="article-quote start" src="{{ URL::asset('img/dquote-start.png') }}">
    @if (isset($title)) <h1 class="title text-center mt-3">{{ $title }}</h1> @endif
    <div class="container article">
        <p class="indent-50">
            {{ $content }}
        </p>
    </div>
    <img class="article-quote end" src="{{ URL::asset('img/dquote-end.png') }}">
</div>

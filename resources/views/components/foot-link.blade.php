<div class="row align-items-end footlink">
    <div class="col-3 d-none d-md-block"></div>
    <div class="col-md-7 col-sm-12">
        <div class="row">
            <div class="col-sm-12 col-md-3 text-center reserve" style="line-height: 1.2em;">
                <div style="font-size: 24px;">Reservations</div>
                <div style="font-size: 56px;">025168679</div>
            </div>
            <div class="col-12 col-md-3 text-right links" style="padding: 0px;">
                <a href="{{ URL::asset('golf-course') }}" class="btn btn-dark btn-lg">สนามกอล์ฟ <i class="fas fa-chevron-right"></i></a>
            </div>
            <div class="col-12 col-md-3 links">
                <a href="{{ URL::asset('room') }}" class="btn btn-dark btn-lg ml-2">ห้องพัก และโรงแรม <i class="fas fa-chevron-right"></i></a>
            </div>
            <div class="col-12 col-md-3 links">
                <a href="{{ URL::asset('contact') }}" class="btn btn-bald btn-gold ml-2"> ติดต่อเรา <i class="fas fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
    <div class="col-2 d-none d-sm-block"></div>
</div>
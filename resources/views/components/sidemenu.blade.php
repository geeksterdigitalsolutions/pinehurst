<div class="sidebar-header">
    <h3>
        <img src="{{ URL::asset('img/logo-cutout.png') }}" alt="" height="40" style="width: 40px;">
        Pinehurst golf club & Hotel
    </h3>
</div>

<ul class="list-unstyled components">
    <li @if ($menu == 'home') class="active" @endif>
        <a href="{{ URL::asset('/home') }}" >{{ trans('menu.home') }}</a>
    </li>
    <li @if ($menu == 'golf') class="active" @endif>
        <a href="{{ URL::asset('/golf') }}" >{{ trans('menu.golf') }}</a>
    </li>
    <li @if ($menu == 'about') class="active" @endif>
        <a href="{{ URL::asset('/about') }}" >{{ trans('menu.about') }}</a>
    </li>
    <li @if ($menu == 'golf-course') class="active" @endif>
        <a href="{{ URL::asset('/golf-course') }}" >{{ trans('menu.golf_course') }}</a>
    </li>
    <li @if ($menu == 'room') class="active" @endif>
        <a href="{{ URL::asset('/room') }}" >{{ trans('menu.room') }}</a>
    </li>
    <li @if ($menu == 'meeting-room') class="active" @endif>
        <a href="{{ URL::asset('/meeting-room') }}" >{{ trans('menu.meeting_room') }}</a>
    </li>
    <li @if ($menu == 'restaurant') class="active" @endif>
        <a href="{{ URL::asset('/restaurant') }}" >{{ trans('menu.restaurant') }}</a>
    </li>
    <li @if ($menu == 'facilities') class="active" @endif>
        <a href="{{ URL::asset('/facilities') }}" >{{ trans('menu.facilities') }}</a>
    </li>
    <li @if ($menu == 'gallery') class="active" @endif>
        <a href="{{ URL::asset('/gallery') }}" >{{ trans('menu.gallery') }}</a>
    </li>
    <li @if ($menu == 'contact') class="active" @endif>
        <a href="{{ URL::asset('/contact') }}" >{{ trans('menu.contact') }}</a>
    </li> 
    <li class="lang">
        <a href="{{ combineQueryString(Request::fullUrl(), ['lang' => 'th']) }}">
            <img src="{{ URL::asset('img/th-24.png') }}" alt="">
        </a>
        <a href="{{ combineQueryString(Request::fullUrl(), ['lang' => 'en']) }}" >
            <img src="{{ URL::asset('img/uk-24.png') }}" alt="">
        </a>
    </li> 
</ul>
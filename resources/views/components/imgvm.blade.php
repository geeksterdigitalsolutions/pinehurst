<div class="imgvm-wrap">
    <img src="{{ $image }}" alt="">
    <div class="capture-hover">
        <h3>{{ $caption }}</h3>
        <a href="{{ $link }}"><span>View More</span>  <i class="fas fa-chevron-right"></i></a>
    </div>
</div>
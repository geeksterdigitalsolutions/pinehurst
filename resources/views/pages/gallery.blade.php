@extends('layouts.app')

@section('content')
<h1 class="text-center title" style="margin-top: 5px;">{{ trans('menu.gallery') }}</h1>
<div class="row mb-4">
    <div class="col-12 text-center">
        <div class="fotorama" data-nav="thumbs" data-fit="cover">
        @foreach($gallery as $row)
            <img src="{{ URL::asset($row['image']) }}">
        @endforeach
        <!--
            <img src="{{ URL::asset('img/gallery/pix_0048.jpg') }}">
            <img src="{{ URL::asset('img/gallery/pix_0047.jpg') }}">
            <img src="{{ URL::asset('img/gallery/pix_0058.jpg') }}">
            <img src="{{ URL::asset('img/gallery/pix_0060.jpg') }}">
            <img src="{{ URL::asset('img/gallery/pix_0061.jpg') }}">
            <img src="{{ URL::asset('img/gallery/pix_0068.jpg') }}">
            <img src="{{ URL::asset('img/gallery/pix_0069.jpg') }}">
            -->
        </div>
    </div>
</div>
@include('components.foot-link')
@endsection
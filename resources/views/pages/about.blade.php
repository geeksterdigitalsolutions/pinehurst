@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <img class="img-head" src="{{ URL::asset('img/about_me.jpg') }}" alt="">
    </div>
</div>
<div class="article-box">
    <img class="article-quote start" src="{{ URL::asset('img/dquote-start.png') }}">
    <h1 class="title text-center mt-3">{{ trans('menu.about') }}</h1>
    <div class="container article">
        <p class="indent-50">
            {{ trans('about.p_1') }}
        </p>
        <p class="indent-50">
            {{ trans('about.p_2') }}
        </p>
        <p class="indent-50">
            {{ trans('about.p_3') }}
        </p>
        <p class="indent-50">
            {{ trans('about.p_4') }}
        </p>
        <p class="indent-50">
            {{ trans('about.p_5') }}
        </p>
    </div>
    <img class="article-quote end" src="{{ URL::asset('img/dquote-end.png') }}">
</div>
@include('components.foot-link')
@endsection
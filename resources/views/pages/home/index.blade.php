@extends('layouts.app')

@section('content')
<div id="homepage">
    <div class="row">
        <div class="col-xs-12 col-sm-12 text-white text-center firstbanner">
            <video class="d-none d-sm-block" autoplay muted loop id="background-video">
                <source src="{{ URL::asset('video/PINEHURST.mp4') }}" type="video/mp4">
            </video>
            <div class="introduce">
                <h1 class="display-4"><span style="border-bottom: 1px solid">Pinehurst golf club & Hotel</span></h1>
                <h2>GOLF CLUB RANGSIT</h2>
            
                <h5 style="margin-top: 60px;">OFFER &amp; PACKAGES</h5>
                <h4 style="margin-top: 20px;"><i class="fas fa-angle-double-down"></i></h4>
                <div class="mt-4 text-right">
                    <a href="{{ URL::asset('/room') }}" class="btn btn-custom">HOTEL</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" style="min-height: 350px;">
        <div class="col-sm-6 col-xs-12 text-white bg-dark box gp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-right">
                        <h1 class="display-4 mb-30">GOLF<br></h1>
                        <a href="{{ URL::asset('/golf') }}" class="btn btn-custom btn-rb">ดูเพิ่มเติม</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12 text-white bg-dark box lp" style="border-left: 2px solid white;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-right">
                        <h1 class="mb-30">HOTEL<br></h1>
                        <a href="{{ URL::asset('/room') }}" class="btn btn-custom btn-rb">ดูเพิ่มเติม</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12 text-white bg-dark box cp" style="border-left: 2px solid white;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-right">
                        <h1 class="mb-30">CATERING<br></h1>
                        <a href="{{ URL::asset('/meeting-room') }}" class="btn btn-custom btn-rb">ดูเพิ่มเติม</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="bg-white" style="padding: 50px 0px;">
        <div class="container">
                @include('components.article', ['title' => trans('home.ph_article.title'), 'content' => trans('home.ph_article.content')])
        </div>
    </div>
    
    {{-- <div class="d-none d-sm-block">
        <div class="row">
            <div class="col-12 text-center mt-4">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 220px; margin: auto;">
                    <li class="nav-item">
                        <a class="nav-link active" id="west-tab" data-toggle="pill" href="#west" role="tab" aria-controls="west" aria-selected="true">West</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="north-tab" data-toggle="pill" href="#north" role="tab" aria-controls="north" aria-selected="false">North</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="south-tab" data-toggle="pill" href="#south" role="tab" aria-controls="south" aria-selected="false">South</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="west" role="tabpanel" aria-labelledby="west-tab">
                        @include('pages.home.west')
                    </div>
                    <div class="tab-pane fade" id="north" role="tabpanel" aria-labelledby="north-tab">
                        @include('pages.home.north')
                    </div>
                    <div class="tab-pane fade" id="south" role="tabpanel" aria-labelledby="south-tab">
                        @include('pages.home.south')
                    </div>
                </div>    
            </div>
        </div>
    </div> --}}

    <div class="container" style="font-family: Arial;">
        {{-- <h1 class="title text-center display-4" >#PinehurstEvent</h1>
        <div class="row event-column">
            <div class="col-xs-12 col-sm-3">
                @component('components.event-tag', ['tag' => 'news', 'img' => URL::asset('img/facilities-01.jpg'), 'link' => '#'])
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                @endcomponent
            </div>
            <div class="col-xs-12 col-sm-3">
                @component('components.event-tag', ['tag' => 'news', 'img' => URL::asset('img/facilities-01.jpg'), 'link' => '#'])
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                @endcomponent
            </div>
            <div class="col-xs-12 col-sm-3">
                @component('components.event-tag', ['tag' => 'event', 'img' => URL::asset('img/facilities-01.jpg'), 'link' => '#'])
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                @endcomponent
            </div>
            <div class="col-xs-12 col-sm-3">
                @component('components.event-tag', ['tag' => 'event', 'img' => URL::asset('img/facilities-01.jpg'), 'link' => '#'])
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                @endcomponent
            </div>
        </div>
        <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="col-6 text-center">
                <a href="#" class="text-darkgold" style="font-size: 28px">VIEW ALL POST <i class="fas fa-angle-right"></i></a>
            </div>
            <div class="col-6 text-center">
                <a href="#" class="text-darkgold" style="font-size: 28px">VIEW ALL EVENT <i class="fas fa-angle-right"></i></a>
            </div>
        </div> --}}
        <div class="row mt-5">
            <div class="col-xs-12 col-sm-4" style="padding-left: 15px;padding-right: 15px;">
                <div class="card">
                    <img class="card-img-top" src="{{ URL::asset('img/home_link_about.jpg') }}" height="195">
                    <div class="card-body">
                        <h1 class="text-center text-gold" style="font-family: 'dsn_montanaregular';">{{ trans('menu.about') }}</h1>
                        <a href="{{ URL::asset('about') }}"><h5 class="text-right">View More</h5></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4" style="padding-left: 15px;padding-right: 15px;">
                <div class="card">
                    <img class="card-img-top" src="{{ URL::asset('img/home_link_gallery.jpg') }}" height="195">
                    <div class="card-body">
                        <h1 class="text-center text-gold" style="font-family: 'dsn_montanaregular';">{{ trans('menu.gallery') }}</h1>
                        <a href="{{ URL::asset('gallery') }}"><h5 class="text-right">View More</h5></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4" style="padding-left: 15px;padding-right: 15px;">
                <div class="card">
                    <img class="card-img-top" src="{{ URL::asset('img/home_link_contact.jpg') }}" height="195">
                    <div class="card-body">
                        <h1 class="text-center text-gold" style="font-family: 'dsn_montanaregular';">{{ trans('menu.contact') }}</h1>
                        <a href="{{ URL::asset('contact') }}"><h5 class="text-right">View More</h5></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('js/jmpress.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.jmslideshow.js') }}"></script>
<script src="{{ URL::asset('js/modernizr.custom.48780.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $('#jms-west-slideshow').jmslideshow();
        $('#jms-north-slideshow').jmslideshow();
        $('#jms-south-slideshow').jmslideshow();
    });
</script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
<style>
    .jms-arrows span.jms-arrows-prev {
        background: #fff url({{ URL::asset('img/arrow_left.png') }}) no-repeat 50% 50%;
    }
    .jms-arrows span.jms-arrows-next {
        background: #fff url({{ URL::asset('img/arrow_right.png') }}) no-repeat 50% 50%;
    }
</style>
@endpush
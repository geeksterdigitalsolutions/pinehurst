<section id="jms-west-slideshow" class="jms-slideshow">
    <div class="step" data-color="bg-white">
        <div class="jms-content">
            <p class="indent-50 text-left">The exellent starting hole gives the golfer an indication of what lies ahead in the next 4 hours. Rolling hills, water geatures and well placed bunkers will all command your attention. Keep the ball slightly right of the fairway centerline to stay out of trouble and enable an easy shot to the green to start the day with a par.</p>
            <a class="jms-link ph-link" href="#">View all courses</a>
        </div>
        <img src="{{ URL::asset('img/facilities-01.jpg') }}" />
    </div>
    <div class="step" data-color="bg-white">
        <div class="jms-content">
            <p class="indent-50 text-left">The exellent starting hole gives the golfer an indication of what lies ahead in the next 4 hours. Rolling hills, water geatures and well placed bunkers will all command your attention. Keep the ball slightly right of the fairway centerline to stay out of trouble and enable an easy shot to the green to start the day with a par.</p>
            <a class="jms-link ph-link" href="#">View all courses</a>
        </div>
        <img src="{{ URL::asset('img/facilities-01.jpg') }}" />
    </div>
    <div class="step" data-color="bg-white">
        <div class="jms-content">
            <p class="indent-50 text-left">The exellent starting hole gives the golfer an indication of what lies ahead in the next 4 hours. Rolling hills, water geatures and well placed bunkers will all command your attention. Keep the ball slightly right of the fairway centerline to stay out of trouble and enable an easy shot to the green to start the day with a par.</p>
            <a class="jms-link ph-link" href="#">View all courses</a>
        </div>
        <img src="{{ URL::asset('img/facilities-01.jpg') }}" />
    </div>
</section>
<div class="text-center wedding">
    <h2 class="text-gold">{{ trans('meetings.wedding-service.title') }}</h2>
    
    @include('components.article', ['content' => trans('meetings.wedding-service.content')])

    <div class="flexCarousel " style="width: 80%; margin: 0px auto;">
        <div class="flexCarousel-container">
            <div class="flexCarousel-slides">
                <div class="flexCarousel-slide">
                    <img src="{{ URL::asset('img/wedding-01.JPG') }}">
                </div>
                <div class="flexCarousel-slide">
                    <img src="{{ URL::asset('img/wedding-02.JPG') }}">
                </div>
                <div class="flexCarousel-slide">
                    <img src="{{ URL::asset('img/wedding-03.JPG') }}">
                </div>
            </div>
        </div>
    </div>

    <div class="text-center promotion">
        {!! trans('meetings.wedding-service.promotion') !!}
    </div>
</div>
<div class="text-center service-fee">
    <h2 class="text-gold">{{ trans('meetings.title') }}</h2>

    <div class="container">
        <table class="table table-striped">
            <tr>
                <td>PALM VIEW BALLROOM</td>
                <td>{{ trans('meetings.fullday') }}</td>
                <td>6,000 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>PALM VIEW BALLROOM</td>
                <td>{{ trans('meetings.halfday') }}</td>
                <td>4,000 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>MEADOW ROOM</td>
                <td>{{ trans('meetings.fullday') }}</td>
                <td>4,000 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>MEADOW ROOM</td>
                <td>{{ trans('meetings.halfday') }}</td>
                <td>3,000 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>GOVERNER ROOM</td>
                <td>{{ trans('meetings.fullday') }}</td>
                <td>3,000 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>GOVERNER ROOM</td>
                <td>{{ trans('meetings.halfday') }}</td>
                <td>2,500 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>FOREST ROOM</td>
                <td>{{ trans('meetings.fullday') }}</td>
                <td>3,000 {{ trans('unit.baht') }}</td>
            </tr>
            <tr>
                <td>FOREST ROOM</td>
                <td>{{ trans('meetings.halfday') }}</td>
                <td>2,000 {{ trans('unit.baht') }}</td>
            </tr>
        </table>
    </div>
</div>
@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <img src="{{ URL::asset('img/meeting-room.jpg') }}" style="height: 400px;" >
    </div>
</div>
@include('components.article', ['title' => trans('meetings.title'), 'content' => trans('meetings.content')])
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 208px; margin: auto;">
    <li class="nav-item">
        <a class="nav-link active" id="service-fee-tab" data-toggle="pill" href="#service-fee" role="tab" aria-controls="service-fee" aria-selected="true">{{ trans('meetings.price') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="wedding-tab" data-toggle="pill" href="#wedding" role="tab" aria-controls="wedding" aria-selected="false">{{ trans('meetings.wedding-service.title') }}</a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="service-fee" role="tabpanel" aria-labelledby="service-fee-tab">
        @include('pages.meeting-room.service-fee')
    </div>
    <div class="tab-pane fade" id="wedding" role="tabpanel" aria-labelledby="wedding-tab">
        @include('pages.meeting-room.wedding')
    </div>
</div>
@include('components.foot-link')
@endsection
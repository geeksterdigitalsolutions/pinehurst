<div class="row mb-4">
  <div class="col-12 text-center">
    <ul class="nav justify-content-center" id="golf-course-slider-north">
      <li class="nav-item">
        <a class="nav-link active" href="#">1</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">2</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">3</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">4</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">5</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">6</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">7</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">8</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">9</a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-12 text-center" style="padding: 0px 10%;">
      <div id="fotorama-north" class="fotorama" data-auto="false" data-width="800" data-maxwidth="100%" data-ratio="9/16">
        <img src="{{ URL::asset('img/golf-course/north/N1.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N2.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N3.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N4.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N5.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N8.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N6.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N7.png') }}">
        <img src="{{ URL::asset('img/golf-course/north/N9.png') }}">
      </div>
  </div>
</div>

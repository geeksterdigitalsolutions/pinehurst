@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 20px; ">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 195px; margin: auto;">
        <li class="nav-item">
            <a class="nav-link active" id="north-tab" data-toggle="pill" href="#north" role="tab" aria-controls="north" aria-selected="true">North</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="west-tab" data-toggle="pill" href="#west" role="tab" aria-controls="west" aria-selected="true">West</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="south-tab" data-toggle="pill" href="#south" role="tab" aria-controls="south" aria-selected="true">South</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="north" role="tabpanel" aria-labelledby="north-tab">
            @include('pages.golf-course.north')
        </div>
        <div class="tab-pane fade" id="west" role="tabpanel" aria-labelledby="west-tab">
            @include('pages.golf-course.west')
        </div>
        <div class="tab-pane fade" id="south" role="tabpanel" aria-labelledby="south-tab">
            @include('pages.golf-course.south')
        </div>
    </div>
</div>

@endsection


@section('script')
<script>
  $(document).ready(function () {

    var $fotoramaNorth = $('#fotorama-north').fotorama();
    var fotoramaNorth = $fotoramaNorth.data('fotorama')
    
    var sildeNumber = $('ul#golf-course-slider-north li')
    $('ul#golf-course-slider-north li a').click(function() {
        if (fotoramaNorth === undefined) {
            $fotoramaNorth = $('#fotorama-north').fotorama();
            fotoramaNorth = $fotoramaNorth.data('fotorama')
        }
        var index = parseInt($(this)[0].innerHTML) - 1
        fotoramaNorth.show(index)
    })

    $fotoramaNorth.on('fotorama:show', function(e, fotorama, extra) {
      var sildeNumberActive = sildeNumber.find('a.active')
      var activedIndex = parseInt(sildeNumberActive[0].innerText) - 1
      if (activedIndex != fotorama.activeIndex) {
        sildeNumberActive.removeClass('active')
        sildeNumber.eq(fotorama.activeIndex).children().addClass('active')
      }
    })

    var $fotoramaWest = $('#fotorama-west').fotorama();
    var fotoramaWest = $fotoramaWest.data('fotorama')

    var sildeNumberWest = $('ul#golf-course-slider-west li')
    $('ul#golf-course-slider-west li a').click(function() {
        if (fotoramaWest === undefined) {
            $fotoramaWest = $('#fotorama-west').fotorama();
            fotoramaWest = $fotoramaWest.data('fotorama')
        }
        var index = parseInt($(this)[0].innerHTML) - 1
        fotoramaWest.show(index)
    })

    $fotoramaWest.on('fotorama:show', function(e, fotorama, extra) {
        var sildeNumberWestActive = sildeNumberWest.find('a.active')
        var activedIndex = parseInt(sildeNumberWestActive[0].innerText) - 1
        if (activedIndex != fotorama.activeIndex) {
            sildeNumberWestActive.removeClass('active')
            sildeNumberWest.eq(fotorama.activeIndex).children().addClass('active')
        }
    })

    var $fotoramaSouth = $('#fotorama-south').fotorama();
    var fotoramaSouth = $fotoramaSouth.data('fotorama')

    var sildeNumberSouth = $('ul#golf-course-slider-south li')
    $('ul#golf-course-slider-south li a').click(function() {
        if (fotoramaSouth === undefined) {
            $fotoramaSouth = $('#fotorama-south').fotorama();
            fotoramaSouth = $fotoramaSouth.data('fotorama')
        }
        var index = parseInt($(this)[0].innerHTML) - 1
        fotoramaSouth.show(index)
    })

    $fotoramaSouth.on('fotorama:show', function(e, fotorama, extra) {
        var sildeNumberSouthActive = sildeNumberSouth.find('a.active')
        var activedIndex = parseInt(sildeNumberSouthActive[0].innerText) - 1
        if (activedIndex != fotorama.activeIndex) {
            sildeNumberSouthActive.removeClass('active')
            sildeNumberSouth.eq(fotorama.activeIndex).children().addClass('active')
        }
    })
  })
</script>
@endsection
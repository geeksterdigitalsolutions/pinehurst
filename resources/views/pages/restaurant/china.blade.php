<h1 class="text-center sub-title">{{ trans('restaurant.tab.china') }}</h1>

<div class="row mb-4">
    <div class="col-12 text-center" style="padding: 0px 10%;">
        <div class="fotorama" data-nav="thumbs" data-fit="cover">
            <img class="img-head" src="{{ URL::asset('img/china-slide-01.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/china-slide-02.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/china-slide-03.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/china-slide-04.jpg') }}">
        </div>
    </div>
</div>
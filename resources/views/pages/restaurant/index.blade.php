@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <img class="img-head" src="{{ URL::asset('img/restaurant.jpg') }}" alt="" height="400">
    </div>
</div>

@include('components.article', ['title' => trans('menu.restaurant'), 'content' => trans('restaurant.content') ])

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 345px; margin: auto;">
    <li class="nav-item">
        <a class="nav-link active" id="first-tab" data-toggle="pill" href="#first" role="tab" aria-controls="first" aria-selected="true">{{ trans('restaurant.tab.19') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="second-tab" data-toggle="pill" href="#second" role="tab" aria-controls="second" aria-selected="false">{{ trans('restaurant.tab.china') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="third-tab" data-toggle="pill" href="#third" role="tab" aria-controls="third" aria-selected="false">{{ trans('restaurant.tab.holeinone') }}</a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="first" role="tabpanel" aria-labelledby="first-tab">
        @include('pages.restaurant.19')
    </div>
    <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
        @include('pages.restaurant.china')
    </div>
    <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
        @include('pages.restaurant.holeinone')
    </div>
</div>
@include('components.foot-link')
@endsection
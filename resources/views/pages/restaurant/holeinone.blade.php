<h1 class="text-center sub-title">{{ trans('restaurant.tab.19') }}</h1>

<div class="row mb-4">
    <div class="col-12 text-center" style="padding: 0px 10%;">
        <div class="fotorama" data-nav="thumbs" data-fit="cover">
            <img class="img-head" src="{{ URL::asset('img/hi1-01.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-02.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-03.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-04.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-05.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-06.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-07.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-08.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-09.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/hi1-10.jpg') }}">
        </div>
    </div>
</div>
<h1 class="text-center sub-title">{{ trans('restaurant.tab.china') }}</h1>

<div class="row mb-4">
    <div class="col-12 text-center" style="padding: 0px 10%;">
        <div class="fotorama" style="height: auto;" data-nav="thumbs" data-fit="cover">
            <img class="img-head" src="{{ URL::asset('img/19thhome-slide-06.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/19thhome-slide-01.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/19thhome-slide-02.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/19thhome-slide-03.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/19thhome-slide-04.jpg') }}">
            <img class="img-head" src="{{ URL::asset('img/19thhome-slide-05.jpg') }}">
        </div>
    </div>
</div>
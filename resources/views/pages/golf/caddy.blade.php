<div class="text-center service-fee">
    <h1 class="text-gold">CADDY</h1>

    <div class="container">
        <table class="table table-striped">
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                <td>{{ trans('golf-course.person_unit', ['price' => '300']) }}</td>
            </tr>
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                <td>{{ trans('golf-course.person_unit', ['price' => '250']) }}</td>
            </tr>
        </table>
    </div>
</div>
<div class="text-center service-fee">
    <h1 class="text-gold">Night Golf</h1>

    <div class="container table-responsive">
        <table class="table table-striped table-golf-fee">
            <tr>
                <td>{{ trans('golf-course.week_day') }}</td>
                <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                <td>{{ trans('golf-course.price', ['price' => '1,450']) }}</td>
            </tr>
            <tr>
                <td>{{ trans('golf-course.week_day') }}</td>
                <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                <td>{{ trans('golf-course.price', ['price' => '750']) }}</td>
            </tr>
            <tr>
                <td>{{ trans('golf-course.holiday') }}</td>
                <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                <td>{{ trans('golf-course.price', ['price' => '1,800']) }}</td>
            </tr>
            <tr>
                <td>{{ trans('golf-course.holiday') }}</td>
                <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                <td>{{ trans('golf-course.price', ['price' => '950']) }}</td>
            </tr>
        </table>
    </div>
</div>
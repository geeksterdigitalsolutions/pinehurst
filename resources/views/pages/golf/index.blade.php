@extends('layouts.app')

@section('content')
<section id="golf">
    <div class="row">
        <div class="col-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-head" src="{{ URL::asset('img/golf.jpg') }}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-head" src="{{ URL::asset('img/night-golf.jpg') }}" alt="Second slide">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 190px; margin: 20px auto;">
        <li class="nav-item">
            <a class="nav-link active" id="thai-tab" data-toggle="pill" href="#thai" role="tab" aria-controls="thai" aria-selected="true">{{ trans('golf-course.type.thai') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="foreigner-tab" data-toggle="pill" href="#foreigner" role="tab" aria-controls="foreigner" aria-selected="false">{{ trans('golf-course.type.foreigner') }}</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent" style=" margin-bottom: 50px;">
        <div class="tab-pane fade show active" id="thai" role="tabpanel" aria-labelledby="thai-tab">
            @include('pages.golf.golf-thai')
        </div>
        <div class="tab-pane fade" id="foreigner" role="tabpanel" aria-labelledby="foreigner-tab">
            @include('pages.golf.golf-foreigner')
        </div>
    </div>
    @include('components.foot-link')
</section>

<section id="night-golf" style="margin-top: 50px;">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 190px; margin: 20px auto;">
        <li class="nav-item">
            <a class="nav-link active" id="night-thai-tab" data-toggle="pill" href="#night-thai" role="tab" aria-controls="night-thai" aria-selected="true">{{ trans('golf-course.type.thai') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="night-foreigner-tab" data-toggle="pill" href="#night-foreigner" role="tab" aria-controls="night-foreigner" aria-selected="false">{{ trans('golf-course.type.foreigner') }}</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent" style=" margin-bottom: 50px;">
        <div class="tab-pane fade show active" id="night-thai" role="tabpanel" aria-labelledby="night-thai-tab">
            @include('pages.golf.night-golf-thai')
        </div>
        <div class="tab-pane fade" id="night-foreigner" role="tabpanel" aria-labelledby="night-foreigner-tab">
            @include('pages.golf.night-golf-foreigner')
        </div>
    </div>
    @include('components.foot-link')
</section>
    
<section id="member-section" style="margin-top: 50px;">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 220px; margin: 20px auto;">
        <li class="nav-item">
            <a class="nav-link active" id="member-tab" data-toggle="pill" href="#member" role="tab" aria-controls="member" aria-selected="true">{{ trans('golf-course.register.tab') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="guest-member-tab" data-toggle="pill" href="#guest-member" role="tab" aria-controls="guest-member" aria-selected="false">{{ trans('golf-course.guest-member') }}</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent" style=" margin-bottom: 50px;">
        <div class="tab-pane fade show active" id="member" role="tabpanel" aria-labelledby="member-tab">
            @include('pages.golf.member')
        </div>
        <div class="tab-pane fade" id="guest-member" role="tabpanel" aria-labelledby="guest-member-tab">
            @include('pages.golf.guest-member')
        </div>
    </div>
    @include('components.foot-link')
</section>



<section id="golf" style="margin-top: 50px;">
    <div class="row">
        <div class="col-12 text-center">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: {{ App::getLocale() == 'th' ? '380px' : '430px' }}; margin: 20px auto;">
                <li class="nav-item">
                    <a class="nav-link active" id="caddy-tab" data-toggle="pill" href="#caddy" role="tab" aria-controls="caddy" aria-selected="true">CADDY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="accessory-tab" data-toggle="pill" href="#accessory" role="tab" aria-controls="accessory" aria-selected="false">ACCESSORY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="golf-set-tab" data-toggle="pill" href="#golf-set" role="tab" aria-controls="golf-set" aria-selected="false">Golf Set</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="shoe-tab" data-toggle="pill" href="#shoe" role="tab" aria-controls="shoe" aria-selected="false">{{ trans('golf-course.shoe') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="umbella-tab" data-toggle="pill" href="#umbella" role="tab" aria-controls="umbella" aria-selected="false">{{ trans('golf-course.umbella') }}</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent" style=" margin-bottom: 50px;">
                <div class="tab-pane fade show active" id="caddy" role="tabpanel" aria-labelledby="caddy-tab">
                    @include('pages.golf.caddy')
                </div>
                <div class="tab-pane fade" id="accessory" role="tabpanel" aria-labelledby="accessory-tab">
                    @include('pages.golf.accessory')
                </div>
                <div class="tab-pane fade" id="golf-set" role="tabpanel" aria-labelledby="golf-set-tab">
                    @include('pages.golf.golf-set')
                </div>
                <div class="tab-pane fade" id="shoe" role="tabpanel" aria-labelledby="shoe-tab">
                    @include('pages.golf.shoe')
                </div>
                <div class="tab-pane fade" id="umbella" role="tabpanel" aria-labelledby="umbella-tab">
                    @include('pages.golf.umbella')
                </div>
            </div>
            @include('components.foot-link')
        </div>
    </div>
</section>
@endsection
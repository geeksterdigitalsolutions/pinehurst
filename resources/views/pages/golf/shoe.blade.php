<div class="text-center service-fee">
    <h1 class="text-gold">{{ trans('golf-course.shoe') }}</h1>

    <div class="container">
        <table class="table table-striped">
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                <td> 150 {{ trans('unit.baht') }}</td>
                
            </tr>
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                <td>150 {{ trans('unit.baht') }}</td>
            </tr>
        </table>
    </div>
</div>
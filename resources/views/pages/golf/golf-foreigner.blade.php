<div class="text-center service-fee">
        <h1 class="text-gold">Green Fee</h1>
    
        <div class="container table-responsive">
            <table class="table table-striped table-golf-fee">
                <tr>
                    <td>{{ trans('golf-course.week_day') }}</td>
                    <td>{{ trans('golf-course.morning') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '2,000']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.week_day') }}</td>
                    <td>{{ trans('golf-course.morning') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '1,000']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.week_day') }}</td>
                    <td>{{ trans('golf-course.after_noon') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '1,400']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.week_day') }}</td>
                    <td>{{ trans('golf-course.after_noon') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '750']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.holiday') }}</td>
                    <td>{{ trans('golf-course.morning') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '2,700']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.holiday') }}</td>
                    <td>{{ trans('golf-course.morning') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '1,400']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.holiday') }}</td>
                    <td>{{ trans('golf-course.after_noon') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '1,800']) }}</td>
                </tr>
                <tr>
                    <td>{{ trans('golf-course.holiday') }}</td>
                    <td>{{ trans('golf-course.after_noon') }}</td>
                    <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                    <td>{{ trans('golf-course.price', ['price' => '950']) }}</td>
                </tr>
            </table>
        </div>
    </div>
<div class="text-center service-fee">
    <h1 class="text-gold">Golf Set</h1>

    <div class="container">
        <table class="table table-striped">
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                <td>{{ trans('golf-course.golf-set', ['price' => '750']) }}</td>
                
            </tr>
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                <td>{{ trans('golf-course.golf-set', ['price' => '375']) }}</td>
            </tr>
        </table>
    </div>
</div>
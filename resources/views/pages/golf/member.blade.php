<div class="text-center service-fee">
    <h1 class="text-gold">Member</h1>

    <div class="container">
        <div class="text-center bg-white" style="padding: 20px 80px">
            {!! trans('golf-course.register.content') !!}
        </div>
    </div>
</div>
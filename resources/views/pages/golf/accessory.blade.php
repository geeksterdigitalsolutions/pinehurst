<div class="text-center service-fee">
    <h1 class="text-gold">รถกอล์ฟ</h1>

    <div class="container">
        <table class="table table-striped">
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 18]) }}</td>
                <td>{{ trans('golf-course.person_pack_unit', ['price' => '700', 'person' => '1']) }}</td>
                <td>{{ trans('golf-course.person_pack_unit', ['price' => '350', 'person' => '2']) }}</td>
            </tr>
            <tr>
                <td>{{ trans('golf-course.holes', ['hole' => 9]) }}</td>
                <td>{{ trans('golf-course.person_pack_unit', ['price' => '350', 'person' => '1']) }}</td>
                <td>{{ trans('golf-course.person_pack_unit', ['price' => '450', 'person' => '2']) }}</td>
            </tr>
        </table>
    </div>
</div>
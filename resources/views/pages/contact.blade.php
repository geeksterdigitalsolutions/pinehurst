@extends('layouts.app')

@section('content')
<div class="container" style="width: 650px;">
    <h1 class="title text-center mt-2">{{ trans('menu.contact') }}</h1>
    <div class="row">
        <div class="col-12">
            <h3>{{ trans('contact.title') }}</h3>
            <p class="indent-50">
                {{ trans('contact.content') }}
            </p>
            <p>
                {!! trans('contact.info') !!}
            </p>
        </div>
    </div>
</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3870.5322311779423!2d100.59842331459575!3d14.045719194199227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2800b728e63cf%3A0xea6efa1cb9516368!2sPinehurst+Golf+%26+Country+Club!5e0!3m2!1sen!2sth!4v1544374208428" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
@endsection
@extends('layouts.master')

@section('body')
<body class="cover">
    <div class="cover-background">
        <div class="container" style="padding: 200px 0px;" style="overflow:hidden">
            <div class="text-center text-white" >
                <img src="{{ URL::asset('img/logo-cutout.png') }}" style="width: 75px;">
                <h1 class="display-3 mt-3 title-cover" style="font-size: 3.5rem">Pinehurst golf club & Hotel</h1>
                <p class="title-cover"> GOLF CLUB RANGSIT </p>
                <a href="{{ URL::asset('home') }}"><button class="btn btn-custom cover-btn">เข้าสู่เว็บไซด์</button></a>
            </div>
        </div>
    </div>
</body>
@endsection
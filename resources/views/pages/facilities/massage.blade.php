<h2 class="text-center title">{{ trans('facilities.tab.massage') }}</h2>

{{-- <div class="flexCarousel">
    <div class="flexCarousel-container">
        <div class="flexCarousel-slides">
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/facilities-01.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/facilities-01.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/facilities-01.jpg') }}">
            </div>
        </div>
    </div>
</div> --}}

<p class="text-center mt-3">
    <p class="text-center mb-0" style="line-height: 1em;">{{ trans('facilities.massage.body') }} 300 {{ trans('unit.baht') }} / {{ trans('unit.hour') }}</p>
    <p class="text-center mb-0" style="line-height: 1em;">{{ trans('facilities.massage.feet') }} 300 {{ trans('unit.baht') }} / {{ trans('unit.hour') }}</p>
    <p class="text-center" style="line-height: 1em;">{{ trans('facilities.massage.oil') }} 500 {{ trans('unit.baht') }} / {{ trans('unit.hour') }}</p>
</p>
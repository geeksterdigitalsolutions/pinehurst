@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/facilities-slide-01.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/facilities-slide-02.jpg" alt="Second slide">
                </div>
            </div>
        </div>
    </div>
</div>

@include('components.article', ['title' => 'FACILITIES', 'content' => trans('facilities.content') ])

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 292px; margin: auto;">
    <li class="nav-item">
        <a class="nav-link active" id="sportclub-tab" data-toggle="pill" href="#sportclub" role="tab" aria-controls="sportclub" aria-selected="true">{{ trans('facilities.tab.sport-club') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="massage-tab" data-toggle="pill" href="#massage" role="tab" aria-controls="massage" aria-selected="false">{{ trans('facilities.tab.massage') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="shop-tab" data-toggle="pill" href="#shop" role="tab" aria-controls="shop" aria-selected="false">{{ trans('facilities.tab.shop') }}</a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="sportclub" role="tabpanel" aria-labelledby="sportclub-tab">
        @include('pages.facilities.sportclub')
    </div>
    <div class="tab-pane fade" id="massage" role="tabpanel" aria-labelledby="massage-tab">
        @include('pages.facilities.massage')
    </div>
    <div class="tab-pane fade" id="shop" role="tabpanel" aria-labelledby="shop-tab">
        @include('pages.facilities.shop')
    </div>
</div>

@include('components.foot-link')

@endsection
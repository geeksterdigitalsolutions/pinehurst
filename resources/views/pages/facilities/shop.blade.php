<h2 class="text-center title">{{ trans('facilities.shop.title') }}</h2>

{{-- <div class="flexCarousel">
    <div class="flexCarousel-container">
        <div class="flexCarousel-slides">
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/facilities-01.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/facilities-01.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/facilities-01.jpg') }}">
            </div>
        </div>
    </div>
</div> --}}

<p class="text-center mt-3">
    {{ trans('facilities.shop.content') }}
</p>
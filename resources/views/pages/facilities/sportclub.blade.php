<h2 class="text-center title">{{ trans('facilities.tab.sport-club') }}</h2>

<div class="flexCarousel" style="width: 80%; margin: 0px auto;">
    <div class="flexCarousel-container">
        <div class="flexCarousel-slides">
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/sport-club-slide-01.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/sport-club-slide-02.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/sport-club-slide-03.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/sport-club-slide-04.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/sport-club-slide-05.jpg') }}">
            </div>
            <div class="flexCarousel-slide">
                <img src="{{ URL::asset('img/sport-club-slide-06.jpg') }}">
            </div>
        </div>
    </div>
</div>

<p class="text-center mt-3">
    {!! trans('facilities.sport-club.content') !!}
</p>
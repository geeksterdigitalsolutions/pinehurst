@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" style="height: 420px;">
                <div class="carousel-item active">
                    <img class="d-none d-sm-block w-100" src="img/room-pinehurst-lodge-slide-01.jpg" alt="First slide">
                    <img class="d-block d-sm-none w-100" src="img/m-room-pinehurst-lodge-slide-01.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-none d-sm-block w-100" src="img/room-pinehurst-lodge-slide-02.jpg" alt="First slide">
                    <img class="d-block d-sm-none w-100" src="img/m-room-pinehurst-lodge-slide-02.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-none d-sm-block w-100" src="img/room-pinehurst-lodge-slide-03.jpg" alt="First slide">
                    <img class="d-block d-sm-none w-100" src="img/m-room-pinehurst-lodge-slide-03.jpg" alt="First slide">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    @include('components.article', ['title' => trans('lodgings.pinehurstlodge.title'), 'content' => trans('lodgings.pinehurstlodge.content')])
</div>

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width: 440px; margin: auto;">
    <li class="nav-item">
        <a class="nav-link active" id="standard-tab" data-toggle="pill" href="#standard" role="tab" aria-controls="standard" aria-selected="true">STANDARD</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="junior-suite-tab" data-toggle="pill" href="#junior-suite" role="tab" aria-controls="junior-suite" aria-selected="false">JUNIOR SULTE</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pinehurst-suite-tab" data-toggle="pill" href="#pinehurst-suite" role="tab" aria-controls="pinehurst-suite" aria-selected="false">PINEHURST SULTE</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="presidental-tab" data-toggle="pill" href="#presidental" role="tab" aria-controls="presidental" aria-selected="false">PRESIDENTAL</a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="standard" role="tabpanel" aria-labelledby="standard-tab">
        <p class="text-center mb-0" style="line-height: 1em;">2,500 {{ trans('unit.baht') }} / {{ trans_choice('unit.person', 2, ['person' => 2]) }}</p>
        <p class="text-center">2,300 {{ trans('unit.baht') }} / {{ trans_choice('unit.person', 1, ['person' => 1]) }}</p>
    </div>
    <div class="tab-pane fade" id="junior-suite" role="tabpanel" aria-labelledby="junior-suite-tab">
        <p class="text-center mb-0" style="line-height: 1em;">5,600 {{ trans('unit.baht') }} / {{ trans_choice('unit.person', 2, ['person' => 2]) }}</p>
        <p class="text-center">5,000 {{ trans('unit.baht') }} / {{ trans_choice('unit.person', 1, ['person' => 1]) }}</p>
    </div>
    <div class="tab-pane fade" id="pinehurst-suite" role="tabpanel" aria-labelledby="pinehurst-suite-tab">
        <p class="text-center">7,000 {{ trans('unit.baht') }}</p>
    </div>
    <div class="tab-pane fade" id="presidental" role="tabpanel" aria-labelledby="presidental-tab">
        <p class="text-center">11,000 {{ trans('unit.baht') }}</p>
    </div>
</div>

<div class="container mt-3">
    <div class="row">
        <div class="col-6 col-xs-12">
            <h2 class="text-gold">{{ trans('services.amenities.title') }}</h2>
            <ul class="dash">
                <li>{{ trans('services.amenities.bed') }}</li>
                <li>{{ trans('services.amenities.refrigerator') }}</li>
                <li>{{ trans('services.amenities.tv') }}</li>
                <li>{{ trans('services.amenities.air') }}</li>
                <li>{{ trans('services.amenities.bathroom') }}</li>
                <li>{{ trans('services.amenities.clothesline') }}</li>
                <li>{{ trans('services.amenities.wardrobe') }}</li>
                <li>{{ trans('services.amenities.slipper') }}</li>
                <li>{{ trans('services.amenities.wifi') }}</li>
                <li>{{ trans('services.amenities.phone') }}</li>
                <li>{{ trans('services.amenities.heater') }}</li>
            </ul>
        </div>
        <div class="col-6 col-xs-12">
            <h2 class="text-gold">{{ trans('services.extra_service.title') }}</h2>
            <ul class="dash">
                <li>{{ trans('services.extra_service.first') }}</li>
                <li>{{ trans('services.extra_service.second') }}</li>
            </ul>
            <h2 class="text-gold">{{ trans('services.securities.title') }}</h2>
            <ul class="dash">
                <li>{{ trans('services.securities.first') }}</li>
                <li>{{ trans('services.securities.second') }}</li>
            </ul>
        </div>
    </div>
</div>
@include('components.foot-link')
@endsection
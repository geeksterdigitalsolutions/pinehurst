@extends('layouts.app')

@section('content')
<div class="row" style="margin-bottom: 75px;">
    <div class="col-12">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block d-sm-none w-100" src="img/m-room-slide-01.jpg" alt="First slide">
                    <img class="d-none d-sm-block w-100" src="img/room-slide-01.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block d-sm-none w-100" src="img/m-room-slide-02.jpg" alt="First slide">
                    <img class="d-none d-sm-block w-100" src="img/room-slide-02.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block d-sm-none w-100" src="img/m-room-slide-03.jpg" alt="First slide">
                    <img class="d-none d-sm-block w-100" src="img/room-slide-03.jpg" alt="First slide">
                </div>
            </div>
        </div>
    </div>
</div>

@include('components.article', ['title' => trans('lodgings.title'), 'content' => trans('lodgings.content')])

<div class="row" style="margin-top: 75px;">
    <div class="col-md-4 col-xs-12">
        @include('components.imgvm', [
            'image' => URL::asset('img/room-plam-view.jpg'),
            'caption' => 'Palm View',
            'link' => URL::asset('room?type=palmview')
        ])
    </div>
    <div class="col-md-4 col-xs-12">
        @include('components.imgvm', [
            'image' => URL::asset('img/room-pinehurst-lodge.jpg'),
            'caption' => 'Pinehurst Lodge',
            'link' => URL::asset('room?type=pinehurstlodge')
        ])
    </div>
    <div class="col-md-4 col-xs-12">
        @include('components.imgvm', [
            'image' => URL::asset('img/room-garden-home.jpg'),
            'caption' => 'Garden Home',
            'link' => URL::asset('room?type=gardenhome')
        ])
    </div>
</div>
@endsection
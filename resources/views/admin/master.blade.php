<?php if (Session::get('admin') == "admin"): ?>
     
<?php else: ?>
      <script>window.location="login";</script>
<?php endif; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PH Manager</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('css/fontawesome/css/fontawesome.min.css') }}">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/square/blue.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset('css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  @stack('styles')
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ URL::asset('manager/event-news') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>P</b>H</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Pine</b>Hurst</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->

  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
            <a href="{{ URL::asset('admingallery') }}">
                <i class="fa fa-book"></i> <span>Gallery</span>
            </a>
        </li>
        <li style="position:fixed; bottom:0;left:0;">
            <a href="{{ URL::asset('/logout') }}">
            <h4 >Log Out</h4>
            </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <br>
    <section class="container text-center">
      <h1>
        จัดการรูปภาพส่วนแกลลอรี่
      </h1>
    </section>
    <br>
    <!-- Main content -->
    <div class="w3-container city" >
                <div class="container">
                  <div class="row align-items-center">
                    <div class="col-12 text-center">
                        <button type="button" class="button button-md" data-toggle="modal" data-target="#addmodal" style="background-color:#009933;color:white;border:0px;padding:20px;border-radius:8px;width:250px;">
                          + เพิ่มรูปภาพ
                        </button>
                    </div>
                  </div>

        <div class="row align-items-center">
            <div class="col-3 text-center">
            </div>
            <div class="col-9 text-center">
              <table class="table table-hover" style="margin-top:50px;max-width:500px;">
                <thead >
                  <tr >
                    <th  style="text-align:center;"><h3>No.</h3></th>
                    <th  style="text-align:center"><h3>Image</h3></th>
                    <th  style="text-align:center"></th>
                  </tr>
                </thead>
                    <tbody>
                        <?php  
                          $i = 0; 
                          $m = 0;  
                        ?>
                        @foreach($gallery as $row)
                        <?php
                          $i++;
                          $m++;
                        ?>
                          <tr>
                            <td style="text-align:center;padding:30px;"><h4 style="margin-top:30px;"><?php echo $i;?></h4></td>
                            <td style="text-align:center;padding:20px;"> <a data-toggle="modal" data-target="#fiximgModal{{$m}}" style="cursor: pointer;">
                            <img src="{{ URL::asset($row['image']) }}" class="img-rounded" alt="Cinque Terre" width="auto" height="100" >
                            </a>
                            </td>
                            <td style="text-align:center">
                                              <form method="post" action="{{ URL::asset('/deletefile') }}">
                                                  {{ csrf_field() }}
                                                <input type="hidden" name="id" id="id" value="{{$row['id']}}"/>
                                                  <button type="submit" onclick="return confirm('คุณต้องการลบรายการนี้ ใช่หรือไม่ ?')" style="background-color:red;border:0;border-radius:5px;margin-top:50px;"><span aria-hidden="true" style="color:white">×</span></button>
                                              </form>
                              </td>
                          </tr>

                                          <div class="modal fade" id="fiximgModal{{$m}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">อัปโหลดภาพใหม่</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                  <div class="modal-body">
                                                        <form method="post" action="{{ URL::asset('/updatefile') }}" enctype="multipart/form-data">
                                                                  {{ csrf_field() }}
                                                                  {{ method_field('POST') }}
                                                            <input type="hidden" name="id" id="id" value="{{$row['id']}}"/>
                                                            <h6 class="text-left"> ขนาดรูปภาพอย่างต่ำมากกว่า *1920 x 1200</h6> <input type="file" name="file" />

                                                      </div>
                                                  <div class="modal-footer">
                                                              <button type="button" class="button button-md button-grey" data-dismiss="modal">Close</button>
                                                              <button type="submit" class="button button-md" style="background-color:#009933;color:white;">Save changes</button>
                                                    </form>
                                                  </div>
                                                </div><!-- end modal-content -->
                                        </div><!-- end modal-dialog -->
                      @endforeach
                    </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content -->
    <div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">อัปโหลดภาพใหม่</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                  </button>
                              </div>
                            <div class="modal-body">
                            <form action="{{ URL::asset('/uploadFile') }}" enctype="multipart/form-data" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <h6> ขนาดรูปภาพอย่างต่ำมากกว่า *1920 x 1200</h6> <input type="file" name="file" />
                            <div class="modal-footer">
                                        <button type="button" class="button button-md button-grey" data-dismiss="modal">Close</button>
                                        <button type="submit" class="button button-md" style="background-color:#009933;color:white;">Save changes</button>
                              </form>
                            </div>
                          </div><!-- end modal-content -->
                        </div><!-- end modal-dialog -->
  </div>

</div>
<!-- ./wrapper -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::asset('plugins/iCheck/icheck.min.js') }} "></script>

@stack('scripts')
</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::group(['middleware' => ['web']], function() {
    Route::get('/', 'PageController@cover');
    Route::get('/home', 'PageController@home');
    Route::get('/golf', 'PageController@golf');
    Route::get('/about', 'PageController@about');
    Route::get('/golf-course', 'PageController@golfCourse');
    Route::get('/room', 'PageController@room');
    Route::get('/meeting-room', 'PageController@meetingRoom');
    Route::get('/restaurant', 'PageController@restaurant');
    Route::get('/facilities', 'PageController@facilities');
    Route::get('/gallery', 'PageController@gallery');
    Route::get('/contact', 'PageController@contact');
    Route::get('/login', 'PageController@login');
    Route::get('/admingallery', 'PageController@admingallery');
    Route::get('/logout', 'PageController@logout');
    Route::post('/check', 'PageController@check');
    Route::post('/uploadFile', 'GalleryController@uploadFile');
    Route::post('/updatefile', 'GalleryController@fiximg');
    Route::post('/deletefile', 'GalleryController@destroy');

    // Route::group(['prefix' => 'manager'], function () {
        
    //     Route::group(['middleware' => ['guest']], function() {
    //         Route::get('', 'AuthController@redirect');
    //         Route::get('login', 'AuthController@index')->name('manager-login');
    //         Route::post('login', 'AuthController@login');
    //     });
        
    //     Route::group(['middleware' => ['auth']], function() {
    //         Route::get('logout', 'AuthController@logout');
    //         Route::resource('event-news', 'EventNewsController');
    //         Route::resource('galleries', 'GalleryController');
    //     });
    // });
// });

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use URL;
use Cookie;
use Crypt;
use Session;
use App\User;
use App\Uploads;

define('ROOM_PV', 'palmview');
define('ROOM_PL', 'pinehurstlodge');
define('ROOM_GH', 'gardenhome');

class PageController extends Controller
{

    public function __construct(Request $request)
    {
        if ($lang = $request->input('lang')) {
            Cookie::queue(Cookie::make('lang', $lang, 360000));
            App::setLocale($lang);
        } else if ($request->cookie('lang')) {
            App::setLocale($request->cookie('lang'));
        }
    }

    public function cover()
    {
        return view('pages.cover');
    }

    public function home()
    {
        return view('pages.home.index', ['menu' => 'home']);
    }

    public function golf()
    {
        return view('pages.golf.index', ['menu' => 'golf']);
    }

    public function about()
    {
        return view('pages.about', ['menu' => 'about']);
    }

    public function golfCourse()
    {
        return view('pages.golf-course.index', ['menu' => 'golf-course']);
    }

    public function room(Request $request)
    {
        $type = $request->query('type');
        switch ($type) {
            case ROOM_PV: return view('pages.room.palm-view', ['menu' => 'room']); break;
            case ROOM_PL: return view('pages.room.pinehurst-lodge', ['menu' => 'room']); break;
            case ROOM_GH: return view('pages.room.garden-home', ['menu' => 'room']); break;
            default: return view('pages.room.index', ['menu' => 'room']);
        }
    }

    public function facilities()
    {
        return view('pages.facilities.index', ['menu' => 'facilities']);
    }

    public function gallery()
    {
        $gallery = Uploads::orderBy('id', 'desc')->get();
        $data   = array('gallery'=>$gallery);
        return view('pages.gallery', ['menu' => 'gallery'])->with($data);
    }

    public function contact()
    {
        return view('pages.contact', ['menu' => 'contact']);
    }

    public function restaurant()
    {
        return view('pages.restaurant.index', ['menu' => 'restaurant']);
    }

    public function meetingRoom()
    {
        return view('pages.meeting-room.index', ['menu' => 'meeting-room']);
    }

    public function login()
    {
      return view('admin.login');

    }
    public function check(Request $request)
    {
      $user = $request->get('user');
      $pass = $request->get('pass');
      //var_dump($user);die;

      $check = User::where('username', $user)
      ->where('password', $pass)
      ->first();
        if (!$check) {
             return back()->with('error','กรุณากรอกข้อมูลให้ถูกต้อง');
        }else{
            session::put('admin','admin');
            return redirect()->action('PageController@admingallery');
        }
    }

    public function admingallery()
    {
        $gallery = Uploads::all()->toArray();
        $data   = array('gallery'=>$gallery);
        //var_dump($data);die;
      return view('admin.master')->with($data);

    }
    public function logout(Request $request) {
        $request->session()->forget('admin');
        return view('pages.cover');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Routes;
use App;
use URL;
use Cookie;
use Crypt;
use Session;
use Input;
use DateTime;
use App\User;
use App\Uploads;



define('ROOM_PV', 'palmview');
define('ROOM_PL', 'pinehurstlodge');
define('ROOM_GH', 'gardenhome');

class GalleryController extends Controller
{

    public function uploadFile()
    {
          //get the file
    $file = Input::file('file');
    //create a file path
    $path = 'uploads/';
    
    //get the file name
    $file_name = $file->getClientOriginalName();

    //save the file to your path
    $file->move($path , $file_name); //( the file path , Name of the file)
    //var_dump($file);die;
    //save that to your database
    $new_file = new Uploads(); 
    $new_file->image = $path . $file_name;
    $new_file->save();
    return redirect()->action('PageController@admingallery');
}

    public function fiximg(Request $request)
    {
        $id = $request->get('id');

        $file = Input::file('file');

        //create a file path
        $path = 'uploads/';
        
        //get the file name
        $file_name = $file->getClientOriginalName();
    
        //save the file to your path
        $file->move($path , $file_name); //( the file path , Name of the file)
        //var_dump($file);die;
        $new_file = $path . $file_name;
        $fiximg = Uploads::where('id', $id)->update(['image' => $new_file ]);

          //var_dump($pd_property);die;
          return back();
        }

        public function destroy(Request $request)
        {
          $id = $request->get('id');
    
          $delete = Uploads::where('id',  $id )->delete();
          return redirect()->action('PageController@admingallery');
        }


}

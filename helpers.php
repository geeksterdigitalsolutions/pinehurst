<?php

function combineQueryString($url = '', $params = [])
{
    $append = http_build_query($params);
    
    $query = parse_url($url, PHP_URL_QUERY);

    return $url . ($query ? "&" : "?") . $append;
}